package framework;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

public class RestAssuredExtension {

    public static RequestSpecification request;

    public RestAssuredExtension() {
        //Arrange
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri(framework.PropertyManager.getInstance().getProperty("Api_Url"));
        builder.setContentType(ContentType.JSON);
        var requestSpec = builder.build();

        request = RestAssured.given().spec(requestSpec)
                .header(new Header("X-Api-Key", PropertyManager.getInstance().getProperty("API_key")));
    }

    public static ResponseOptions<Response> getOps(String url) {
        //Act
        return request.get(url);
    }

    public static ResponseOptions<Response> postOpsWithBody(String url, String body) {
        //Act
        request.body(body);
        return request.post(url);
    }
}