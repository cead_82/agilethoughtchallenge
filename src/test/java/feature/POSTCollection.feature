Feature: POST operation for Collections

  Scenario: Create a collection
    Given I perform POST operation for "/collections" with body
      |file        |
      |BodyPost.txt|
    Then I should see the body has name as "Sample Collection 632"