package steps;

import framework.RestAssuredExtension;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GETApiKeyOwnerSteps implements En{
    private static ResponseOptions<Response> response;

    public GETApiKeyOwnerSteps(){
        Given("^I perform GET operation for \"([^\"]*)\"$", (String uri) -> {
            response = RestAssuredExtension.getOps(uri);
        });

        Then("^I should see userid number as \"([^\"]*)\"$", (String userIdNumber) -> {
            assertThat(response.getBody().jsonPath().get("user.id"), is(Integer.parseInt(userIdNumber)));
        });
    }
}