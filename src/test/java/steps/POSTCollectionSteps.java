package steps;

import framework.RestAssuredExtension;
import io.cucumber.datatable.DataTable;
import io.cucumber.java8.En;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class POSTCollectionSteps implements En {
    private static ResponseOptions<Response> response;

    public POSTCollectionSteps(){
        Given("^I perform POST operation for \"([^\"]*)\" with body$", (String url, DataTable table) -> {
            List<List<String>> data = table.asLists();
            String path = System.getProperty("user.dir") + "/src/test/resources/"+ data.get(1).get(0);
            String body = Files.readString(Paths.get(path));
            response = RestAssuredExtension.postOpsWithBody(url, body);
        });

        Then("^I should see the body has name as \"([^\"]*)\"$", (String name) -> {
            assertThat(response.getBody().jsonPath().get("collection.name"), is(name));
        });
    }
}
